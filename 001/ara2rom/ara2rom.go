package main

import (
    "os"
    "fmt"
    "strconv"
    "bitbucket.org/jt2k/ori-language-learning/001/romannumerals"
)

func main() {
    if len(os.Args) < 2 {
        fmt.Println("Please specify a number to convert")
        os.Exit(1)
    }
    arabic, err := strconv.Atoi(os.Args[1])
    if (err != nil) {
        fmt.Println("Not a valid number")
        os.Exit(1)
    }
    fmt.Println(romannumerals.ArabicToRoman(arabic))
}
