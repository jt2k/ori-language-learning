package main

import (
    "os"
    "fmt"
    "bitbucket.org/jt2k/ori-language-learning/001/romannumerals"
)

func main() {
    if len(os.Args) < 2 {
        fmt.Println("Please specify a string to convert")
        os.Exit(1)
    }
    fmt.Println(romannumerals.RomanToArabic(os.Args[1]))
}
