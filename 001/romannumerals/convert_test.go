package romannumerals

import "testing"

func TestConvert(t *testing.T) {
    cases := []struct {
        arabic int
        roman string
    }{
        {1, "I"},
        {3, "III"},
        {4, "IV"},
        {5, "V"},
        {7, "VII"},
        {9, "IX"},
        {10, "X"},
        {14, "XIV"},
        {15, "XV"},
        {16, "XVI"},
        {19, "XIX"},
        {20, "XX"},
        {23, "XXIII"},
        {24, "XXIV"},
        {35, "XXXV"},
        {40, "XL"},
        {99, "XCIX"},
        {445, "CDXLV"},
        {2367, "MMCCCLXVII"},
        {3999, "MMMCMXCIX"},
    }
    for _, c := range cases {
        roman := ArabicToRoman(c.arabic)
        if roman != c.roman {
            t.Errorf("ArabicToRoman(%d) == %q, want %q", c.arabic, roman, c.roman)
        }
        arabic := RomanToArabic(c.roman)
        if arabic != c.arabic {
            t.Errorf("RomanToArabic(%q) == %d, want %d", c.roman, arabic, c.arabic)
        }
    }
}
