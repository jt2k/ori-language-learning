package romannumerals

var mapping = []struct{
    one, five, ten string
    multiplier int
}{
    {"C", "D", "M", 100},
    {"X", "L", "C", 10},
    {"I", "V", "X", 1},
}

func ArabicToRoman(arabic int) string {
    roman := ""
    for _, m := range mapping {
        for arabic >= 10 * m.multiplier {
            roman += m.ten
            arabic -= 10 * m.multiplier
        }
        if arabic >= 9 * m.multiplier {
            roman += m.one + m.ten
            arabic -= 9 * m.multiplier
        }
        if arabic >= 5 * m.multiplier {
            roman += m.five
            arabic -= 5 * m.multiplier
        }
        if arabic >= 4 * m.multiplier {
            roman += m.one + m.five
            arabic -= 4 * m.multiplier
        }
    }
    for arabic > 0 {
        roman += "I"
        arabic -= 1
    }
    return roman
}

func RomanToArabic(roman string) int {
    arabic := 0
    for _, m := range mapping {
        for len(roman) > 0 && roman[0:1] == m.ten {
            arabic += 10 * m.multiplier
            roman = roman[1:]
        }
        if len(roman) > 1 && roman[0:2] == m.one + m.ten {
            arabic += 9 * m.multiplier
            roman = roman[2:]
        }
        if len(roman) > 0 && roman[0:1] == m.five {
            arabic += 5 * m.multiplier
            roman = roman[1:]
        }
        if len(roman) > 1 && roman[0:2] == m.one + m.five {
            arabic += 4 * m.multiplier
            roman = roman[2:]
        }
    }
    for len(roman) > 0 && roman[0:1] == "I" {
        arabic += 1
        roman = roman[1:]
    }
    return arabic
}
